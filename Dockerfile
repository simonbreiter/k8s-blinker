FROM python:3

WORKDIR /usr/src/app

COPY blinker.py ./

RUN pip install rpi.gpio

RUN chmod +x ./blinker.py

CMD ["python","./blinker.py"] 
